# Table Prediction

* **predict_only.ipynb**: Notebook to predict, whether a page contains a table or not.

* **best_model.h5**: VGG16 trained for table detection

* **to_predict.csv**: list of pages for for which to make a prediction

predict\_only.ipynb reads the list of pages for which to make the predictions, predicts and writes the results to Prediction.csv. For this 'predict\_file_path'  needs to point fo the directory where the pageimages are to be found.